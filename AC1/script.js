let colorWell;
const defaultColor = "#0000ff";


window.addEventListener("load", startup, false);

function startup() {
    colorWell = document.querySelector("#colorWell");
    colorWell.value = defaultColor;
    colorWell.addEventListener("input", updateFirst, false);
    colorWell.select();
}

function updateFirst(event) {
    const div = document.querySelector("div");
    if (div) {
        div.style.backgroundColor = event.target.value;
    }

}

function printaCarro() {
    var x = document.getElementById("carros").value;
    document.getElementById("modelo_carro").innerHTML = x;
}

function adicionaMontadora() {
    var y = document.getElementById("carros");
    var option = document.createElement("option");
    option.text = document.getElementById("novaMontadora").value;
    y.add(option);
}

function excluiMontadora() {
    var z = document.getElementById("carros");
    z.remove(z.selectedIndex);
}

function easterEgg() {
    document.body.style.backgroundImage = "url('x.jpeg')";
}
